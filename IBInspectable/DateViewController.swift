//
//  DateViewController.swift
//  IBInspectable
//
//  Created by M R on 8/1/17.
//  Copyright © 2017 M R. All rights reserved.
//

import UIKit

class DateViewController: UIViewController {

    @IBOutlet weak var dateTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func setDate(_ sender: Any) {
        DatePickerDialog().show("Date Picker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date){
            (date) -> Void in
            
            let components = Calendar.current.dateComponents([.day, .month, .year], from: date!)
            if let day = components.day, let month = components.month, let year = components.year {
                self.dateTextField.text = "\(day) - \(month) - \(year)"
            }
            
        }
        
       
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
