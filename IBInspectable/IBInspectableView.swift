//
//  IBInspectableView.swift
//  IBInspectable
//
//  Created by M R on 8/1/17.
//  Copyright © 2017 M R. All rights reserved.
//

import UIKit

class IBInspectableView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    // Background color
    @IBInspectable var backColor: UIColor?{
        didSet{
            backgroundColor = backColor
        }
    }
    
    // Corner redius
    @IBInspectable var corRedius : CGFloat = 0{
        didSet {
            layer.cornerRadius = corRedius
            layer.masksToBounds = corRedius > 0
        }
    }

    
    // Border width
    @IBInspectable var borWidth : CGFloat = 0{
        didSet {
            layer.borderWidth = borWidth
            layer.masksToBounds = borWidth > 0
        }
    }
    
    // Border Color
    @IBInspectable var borColor: UIColor?{
        didSet{
            layer.borderColor = borColor?.cgColor
        }
    }
}
